@extends('layout/main')

@section('content')
<div class="container-fluid">
<h3>PRODUK</h3>
    <div class="row">
        @forelse ($produk as $prd)
        <div class="col-md-3">
            <div class="card">
                <img class="card-img-top" src="{{ Storage::url('public/img/').$prd->foto }}" alt="Foto produk">
                <div class="card-body">
                    <h5 class="card-title">{{ $prd->nama }}</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $prd->kategori }}</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-text">Rp. {{ $prd->harga }}</h5>
                </div>
                <div class="card-body">
                    <h5 class="card-text">{{ $prd->detail }}</h5>
                </div>
                <div class="card-body text-center">
                    <btn class="card-text">Beli</btn>
                </div>
            </div>
        </div>
        @empty
        <div class="col-md-12 alert alert-danger">
            Data Produk belum Tersedia.
        </div>
        @endforelse
    </div>
    <!-- <a href="#" class="btn btn-warning my-4 text-white">LIHAT SELENGKAPNYA</a> -->
</div>
@endsection