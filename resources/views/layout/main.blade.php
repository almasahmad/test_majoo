<!doctype html>
<html lang="en">
  <head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{url('assets/bootstrap.min.css')}}">

	<!-- Font Awesome -->
	<script src="{{url('assets/a076d05399.js')}}"></script>

	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="{{url('assets/jquery-3.5.1.slim.min.js')}}"></script>
	<script src="{{url('assets/bootstrap.bundle.min.js')}}"></script>

	<title>Toko Sejahtera</title>
  </head>
  <body>
	<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
		<div class="container">
			<a class="navbar-brand" href="/">Majoo Teknologi Indonesia</a>
		</div>
	</nav>

	@yield('content')
	
	<div class="container-fluid bg-dark text-white mt-5 text-center">
		<div class="text-center">
			<h5>2019 @ PT Majoo Teknologi Indonesia</h5>
		</div>
	</div>
  </body>
</html>