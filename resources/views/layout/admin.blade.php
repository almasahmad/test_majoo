<!doctype html>
<html lang="en">
  <head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{url('assets/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">

	<!-- Font Awesome -->
	<script src="{{url('assets/a076d05399.js')}}"></script>

	<!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
	<script src="{{url('assets/jquery-3.5.1.slim.min.js')}}"></script>
	<script src="{{url('assets/bootstrap.bundle.min.js')}}"></script>

	<title>Toko Sejahtera | Admin</title>
  </head>
  <body>
	<nav class="navbar navbar-expand-md navbar-dark bg-warning">
    <div class="container">
        <a class="navbar-brand" href="/admin">ADMIN</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav mt-2 mt-lg-0 ml-auto">
                <li class="nav-item @yield('produk')">
                    <a class="nav-link" href="/admin/view">Tambah Produk</a>
                </li>
                <li class="nav-item @yield('kategori')">
                    <a class="nav-link" href="/admin/kategori">Tambah Kategori</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/admin/logout">Logout</a>
                </li>
            </ul>
        </div>
    </div>
    </nav>
    @yield('content')
  </body>
</html>