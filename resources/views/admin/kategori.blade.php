@extends('layout/admin')
@section('kategori','active')

@section('content')
<div class="container">
    <h3 class="my-3">DATA KATEGORI</h3>
    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#uploadModal">Tambah Kategori</a>
    <table class="table table-bordered mt-1" id="myTable">
        <thead>
            <tr>
                <th>no</th>
                <th>Nama Kategori</th>
                <th>Deskripsi</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($kategori as $a)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $a -> nama }}</td>
                <td style="width: 40%">{{ $a -> deskripsi }}</td>
                <td style="width: 10%">
                    <a class="btn btn-sm btn-info" href="/admin/kategori/edit/{{$a->id}}">Edit</a>
                    <form class="mt-2" action="/admin/kategori/produk/{{$a->id}}" method="post">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6"><h6 class="text-center">DATA PRODUK BELUM TERSEDIA</h6></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Upload File -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/admin/kategori/tambah" method="post">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <label>Nama</label>
                    <input id="id_fat" type="text" class="form-control" name="nama" required autofocus>
                    <label>deskripsi</label>
                    <input id="kapasitas" type="text" class="form-control" name="deskripsi" required autofocus>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endsection