@extends('layout/admin')

@section('content')
<div class="container">
    <h3 class="my-3">EDIT KATEGORI</h3>
    <form  class="w-75" action="/admin/kategori/edit/{{$kategori->id}}" method="post" enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="modal-body"> 
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="nama" value="{{$kategori->nama}}">
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" name="deskripsi" rows="3">{{$kategori->deskripsi}}</textarea>
            </div>
            <div class="form-group">
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a class="btn btn-secondary" data-dismiss="modal" href="/admin/kategori">Tutup</a>
    </form>
</div>
@endsection