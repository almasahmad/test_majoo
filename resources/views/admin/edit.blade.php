@extends('layout/admin')

@section('content')
<div class="container">
    <h3 class="my-3">EDIT PRODUK</h3>
    <form  class="w-75" action="/admin/edit/{{$produk->id}}" method="post" enctype="multipart/form-data">
        @method('put')
        @csrf
        <div class="modal-body"> 
            <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$produk->nama}}">
            </div>
            <div class="form-group">
            <label>Detail</label>
            <textarea class="form-control" name="detail" rows="3">{{$produk->detail}}</textarea>
            </div>
            <div class="form-group">
            <label>Harga</label>
            <input type="number" min="0" class="form-control" name="harga" value="{{$produk->harga}}">
            </div>
            <select id="kategori" name="kategori" class="form-control selectpicker" data-live-search="true" value="{{$produk->kategori}}">
                <option value="a">a</option>
            </select>
            <label>Foto</label>
            <input type="file" class="form-control-file" name="foto">
        </div>
        <button type="submit" class="btn btn-success">Simpan</button>
        <a class="btn btn-secondary" data-dismiss="modal" href="/admin/view">Tutup</a>
    </form>
</div>
@endsection