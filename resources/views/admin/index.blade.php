@extends('layout/admin')
@section('produk','active')

@section('content')
<div class="container">
    @if (session('status'))
    <div class="alert alert-succes my-3">
        {{ session('status') }}
    </div>
    @endif
    <h3 class="my-3">DATA PRODUK</h3>
    <a class="btn btn-success" href="#" data-toggle="modal" data-target="#uploadModal">Tambah Produk</a>
    <table class="table table-bordered mt-1" id="myTable">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kategori</th>
                <th>Detail</th>
                <th>Harga</th>
                <th>Foto</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($produk as $prd)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $prd->nama }}</td>
                <td style="width: 40%">{{ $prd->kategori }}</td>
                <td>{{ $prd->detail }}</td>
                <td>{{ $prd->harga }}</td>
                <td><img src="{{ Storage::url('public/img/').$prd->foto }}" alt="Foto produk" width="100"></td>
                <td style="width: 10%">
                    <a class="btn btn-sm btn-info" href="/admin/edit/{{$prd->id}}">Edit</a>
                    <form class="mt-2" action="/admin/produk/{{$prd->id}}" method="post">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @empty
            <tr>
                <td colspan="6"><h6 class="text-center">DATA PRODUK BELUM TERSEDIA</h6></td>
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

<!-- Upload File -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="/admin/tambah" method="post" enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
                </div>
                <div class="modal-body">
                    @csrf
                    <label>Nama</label>
                    <input id="name" type="text" class="form-control" name="nama" required autofocus>
                    <label>Detail</label>
                    <input id="detail" type="text" class="form-control" name="detail" required autofocus>
                    <label>Harga</label>
                    <input id="harga" type="number" class="form-control" name="harga" required autofocus>
                    <label>Kategori</label>
                    <select id="kategori" name="kategori" class="form-control selectpicker" data-live-search="true">
                        @foreach ($kategori as $a)
                            <option value="{{ $a -> nama }}">{{ $a-> nama }}</option>
                        @endforeach
                    </select>
                    <label>Foto</label>
                    <input type="file" class="form-control-file" name="foto" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endsection