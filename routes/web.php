<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/login', 'AuthController@loginview');
Route::get('/proseslogin', 'AuthController@proseslogin');
Route::get('/admin/logout', 'AuthController@logout');

Route::get('/user', 'UserController@index');

Route::middleware(['auth'])->group(function () {
    Route::get('/admin/view', 'AdminController@index');
    Route::post('/admin/tambah', 'AdminController@store');
    Route::get('/admin/edit/{produk}', 'AdminController@update');
    Route::put('/admin/edit/{produk}', 'AdminController@updateProses');
    Route::delete('/admin/produk/{produk}', 'AdminController@delete');

    Route::get('/admin/kategori', 'KategoriController@index');
    Route::post('/admin/kategori/tambah', 'KategoriController@store');
    Route::get('/admin/kategori/edit/{kategori}', 'KategoriController@update');
    Route::put('/admin/kategori/edit/{kategori}', 'KategoriController@updateProses');
    Route::delete('/admin/kategori/produk/{kategori}', 'KategoriController@delete');
});