<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function index(){
        $kategori = Kategori::all();
        return view('admin/kategori') -> with(compact('kategori'));
    }

    public function store(Request $request){
        // $request->validate([
        //     'nama' => 'required',
        //     'deskripsi' => 'required',
        // ]);

        Kategori::create([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi
        ]);

        return redirect('/admin/kategori') -> with('status','Berhasil Menambahkan');
    }

    public function update(Kategori $kategori){
        return view('admin/editKategori') -> with(compact('kategori'));
    }

    public function updateProses(Request $request, Kategori $kategori){
        // $request->validate([
        //     'nama' => 'required',
        //     'detail' => 'required',
        //     'harga' => 'required',
        //     'foto' => 'required|image',
        // ]);

        Kategori::where('id', $kategori->id)->update([
            'nama' => $request->nama,
            'deskripsi' => $request->deskripsi
        ]);

        return redirect('/admin/kategori');
    }

    public function delete(Kategori $kategori){
        Kategori::destroy($kategori->id);
        return redirect('/admin/kategori');
    }
}
