<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
// use DB;
use App\Models\Produk;
use App\Models\Kategori;

class AdminController extends Controller
{
    public function index(){
        $produk = Produk::all();
        $kategori = Kategori::all();
        // return $produk;
        return view('admin/index') 
            -> with(compact('produk'))
            -> with(compact('kategori'));
    }

    public function store(Request $request){
        
        // $request->validate([
        //     'nama' => 'required',
        //     'detail' => 'required',
        //     'harga' => 'required',
        //     'foto' => 'required|image',
        // ]);

        $image = $request->file('foto');
        $image->storeAs('/public/img/', $image->hashName());

        Produk::create([
            'nama' => $request->nama,
            'detail' => $request->detail,
            'harga' => $request->harga,
            'kategori' => $request->kategori,
            'foto' => $image->hashName()
        ]);

        return redirect('/admin/view') -> with('status','Berhasil Menambahkan');
    }

    public function update(Produk $produk){
        return view('admin/edit') -> with(compact('produk'));
    }

    public function updateProses(Request $request, Produk $produk){
        // $request->validate([
        //     'nama' => 'required',
        //     'detail' => 'required',
        //     'harga' => 'required',
        //     'foto' => 'required|image',
        // ]);

        Produk::where('id', $produk->id)->update([
            'nama' => $request->nama,
            'detail' => $request->detail,
            'harga' => $request->harga,
            'kategori' => $request->kategori,
        ]);

        return redirect('/admin/view');
    }

    public function delete(Produk $produk){
        Produk::destroy($produk->id);
        return redirect('/admin/view');
    }
}
