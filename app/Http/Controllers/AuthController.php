<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginview(){
        return view('admin/login');
    }
    
    public function proseslogin(Request $request)
    {
        if(Auth::attempt($request->only('name','password'))){
            return redirect('/admin/view');
        }else{
            return redirect('/login')->with('status','Login gagal, periksa username dan password');
        }
        return redirect('/admin/view');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
