<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Produk;

class UserController extends Controller
{
    public function index(){
        $produk = Produk::all();
        return view('index') -> with(compact('produk'));
    }
}
